import * as firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyB4HZv8K4XjY8Uzx5rrHPS7eX6QtlnauDw",
  authDomain: "resume-react-fd326.firebaseapp.com",
  databaseURL: "https://resume-react-fd326.firebaseio.com",
  projectId: "resume-react-fd326",
  storageBucket: "resume-react-fd326.appspot.com",
  messagingSenderId: "241332365954",
  appId: "1:241332365954:web:27f685fce461c05265229b",
};
// Initialize Firebase
// firebase.initializeApp(firebaseConfig);

export const fire = firebase.initializeApp(firebaseConfig);

export const dbRef = firebase.database().ref();
