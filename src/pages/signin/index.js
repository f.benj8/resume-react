import React from "react";
import SignInSide from "../../components/signin/SignInSide";

function SignIn() {
  return (
    <div className="App">
      <SignInSide />
    </div>
  );
}

export default SignIn;
