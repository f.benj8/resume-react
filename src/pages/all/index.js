import * as firebase from "firebase";
// import { dbRef } from "../../config/contants";
import React, { Component } from "react";
import SignInSide from "../../components/signin/SignInSide";

export default class All extends Component {
  constructor() {
    super();
    this.state = {
      name: "Resume",
    };
  }
  componentDidMount() {
    const rootRef = firebase.database().ref().child("resume-react");
    const nameRef = rootRef.child("name");
    nameRef.on("value", (snap) => {
      this.setState({
        name: snap.val(),
      });
    });
    // this.writeNewPost('1','2','3','4','5');
  }

  writeNewPost(uid, username, picture, title, body) {
    // A post entry.
    var postData = {
      author: username,
      uid: uid,
      body: body,
      title: title,
      starCount: 0,
      authorPic: picture,
    };

    // Get a key for a new Post.
    var newPostKey = firebase.database().ref().child("resume-react").push().key;

    // Write the new post's data simultaneously in the posts list and the user's post list.
    var updates = {};
    updates["/posts/" + newPostKey] = postData;
    // updates['/user-posts/' + uid + '/' + newPostKey] = postData;

    return firebase.database().ref().update(updates);
  }

  render() {
    return (
      <div className="App">
        <h1>{this.state.name}</h1>
        <SignInSide />
      </div>
    );
  }
}
