import React, { Component } from "react";
import { dbRef } from "../../config/contants";
import * as firebase from "firebase";

class Resume extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: null,
    };
  }

  componentDidMount() {
    this.read();
  }
  read() {
    let resume = dbRef.child("resume-react");
    resume.on("value", (snap) => {
      //   this.setState({
      //     name: snap.val(),
      //   });
      console.log(snap.val());
    });
  }
  write() {
    firebase.database().ref("resume-react").set({
      name: "text",
      tel: 777
    });
  }

  render() {
    return (
      <div>
        Name : {this.state.name}
        <button onClick={this.write}>submit</button>{" "}
      </div>
    );
  }
}
export default Resume;
