import React from "react";
import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
// import SignInSide from "./SignInSide";
import SignIn from "./pages/signin";
import Resume from "./pages/resume";
import All from "./pages/all";
import Auth from "./pages/auth";

// function App() {
//   return (
//     <div className="App">
//       <Switch>
//         <Route exact path="/signin" component={SignIn} />
//       </Switch>
//       <SignInSide />
//     </div>
//   );
// }

const App = () => (
  <div>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={SignIn} />
        <Route exact path="/signin" component={SignIn} />
        <Route exact path="/resume" component={Resume} />
        <Route exact path="/all" component={All} />
        <Route exact path="/auth" component={Auth} />
      </Switch>
    </BrowserRouter>
  </div>
);

export default App;
